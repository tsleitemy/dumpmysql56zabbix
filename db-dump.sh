#!/bin/bash

PASSWORD=<SENHA>
USER=<NOME>
DATABASE=<SCHEMA>
DB_FILE=dump$(date +%d-%m-%Y_%H_%M_%S).sql
DIR=/tmp

EXCLUDED_TABLES=(
history_uint
history
history_text
trends
events
history_str
alerts
event_recovery
trends_uint
housekeeper
)

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

echo "Dump Estrutura"
mysqldump  --user=${USER} -p --single-transaction --no-data --routines ${DATABASE} > ${DIR}/${DB_FILE}

echo "Dump Contetudo"
mysqldump  --user=${USER} -p  ${DATABASE} --no-create-info --skip-triggers ${IGNORED_TABLES_STRING} >> ${DIR}/${DB_FILE}


mysqldump  --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info --skip-triggers ${IGNORED_TABLES_STRING} >> ${DB_FILE}
echo "Dump (Contetudo) - OK " 